## Aplicación del Clima - Curso Node

Esta es una aplicación para ver el clima de una ciudad.

Ejercicio del curso de Node de Fernando Herrera en Udemy disponible en:
[https://www.udemy.com/course/node-de-cero-a-experto/](https://www.udemy.com/course/node-de-cero-a-experto/)

Recueren ejecutar `npm install` para las librerías.

### Ejemplo:

```
node app -d "New York"
```
