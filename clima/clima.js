const axios = require("axios");

const { API_KEY_CLIMA } = require("../config/config");

const getClima = async (lat, long) => {
  const resp = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${long}&appid=${API_KEY_CLIMA}&units=metric`
  );

  if (resp.data.cod !== 200) {
    throw new Error(
      "No hay resultados para la latitud y longitud especificada"
    );
  }

  return resp.data.main.temp;
};

module.exports = {
  getClima,
};
