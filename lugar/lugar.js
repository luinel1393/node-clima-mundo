const axios = require("axios");

const { API_KEY_CLIMA } = require("../config/config");

const getLugarLatLng = async (dir) => {
  const encodeUrl = encodeURI(dir);

  const resp = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${encodeUrl}&appid=${API_KEY_CLIMA}`
  );

  if (resp.data.cod !== 200) {
    throw new Error(`No hay resultados para ${dir}`);
  }

  const data = resp.data;
  const direccion = data.name;
  const lat = data.coord.lat;
  const long = data.coord.lon;

  return {
    direccion,
    lat,
    long,
  };
};

module.exports = {
  getLugarLatLng,
};
